# Slides

The slides are based on metropolis (mtheme) <https://github.com/matze/mtheme> and are compatible with knitr. Refer to metropolis' documentation for building the `.sty` files.

## Compile

Compile using

```
Rscript -e "knitr::knit('%.Rnw')"|lualatex -interaction=nonstopmode %.tex|bibtex %.aux|lualatex -interaction=nonstopmode %.tex
```
